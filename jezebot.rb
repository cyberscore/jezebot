require 'bundler/setup'

require 'cinch'
require 'cinch/plugins/cyberscore'
require 'uri'

bot = Cinch::Bot.new do
  configure do |c|
    c.server   = "irc.surrealchat.net"

    c.nick     = "JezeBot"
    c.password = ENV['BOT_PASSWORD']
    c.channels = [ "#cyberscore" ]

    c.plugins.plugins = [ Cinch::Plugins::Cyberscore ]
  end

end

bot.start